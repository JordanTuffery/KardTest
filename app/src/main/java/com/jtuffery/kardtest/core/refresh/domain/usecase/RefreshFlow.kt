package com.jtuffery.kardtest.core.refresh.domain.usecase

import com.jtuffery.kardtest.core.refresh.domain.manager.RefreshManager
import kotlinx.coroutines.flow.Flow

interface RefreshFlow : Flow<Unit>

fun getRefreshFlow(manager: RefreshManager): RefreshFlow =
    object : RefreshFlow, Flow<Unit> by manager.refreshFlow {}