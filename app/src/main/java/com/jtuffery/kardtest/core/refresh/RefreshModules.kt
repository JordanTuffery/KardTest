package com.jtuffery.kardtest.core.refresh

import com.jtuffery.kardtest.core.refresh.domain.manager.DefaultRefreshManager
import com.jtuffery.kardtest.core.refresh.domain.manager.RefreshManager
import com.jtuffery.kardtest.core.refresh.domain.usecase.getRefreshFlow
import com.jtuffery.kardtest.core.refresh.domain.usecase.refreshAppUseCaseFactory
import com.jtuffery.kardtest.core.refresh.presentation.RefreshViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val refreshDomainModule = module {
    single<RefreshManager> { DefaultRefreshManager() }

    factory { getRefreshFlow(get()) }
    factory { refreshAppUseCaseFactory(get()) }
}

val refreshPresentationModule = module {
    viewModel { RefreshViewModel(get()) }
}

val refreshModules = refreshDomainModule + refreshPresentationModule