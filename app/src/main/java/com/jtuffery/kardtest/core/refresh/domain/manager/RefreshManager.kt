package com.jtuffery.kardtest.core.refresh.domain.manager

import kotlinx.coroutines.flow.Flow

interface RefreshManager {
    val refreshFlow: Flow<Unit>
    fun refresh()
}