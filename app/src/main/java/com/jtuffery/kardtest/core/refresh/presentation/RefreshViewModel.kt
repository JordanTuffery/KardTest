package com.jtuffery.kardtest.core.refresh.presentation

import androidx.lifecycle.ViewModel
import com.jtuffery.kardtest.core.refresh.domain.usecase.RefreshAppUseCase

class RefreshViewModel(
    private val refreshAppUseCase: RefreshAppUseCase
): ViewModel() {
    fun refresh() {
        refreshAppUseCase()
    }
}