package com.jtuffery.kardtest.core.refresh.domain.manager

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow

class DefaultRefreshManager: RefreshManager {
    override val refreshFlow = MutableSharedFlow<Unit>(0, 1)

    override fun refresh() {
        refreshFlow.tryEmit(Unit)
    }
}