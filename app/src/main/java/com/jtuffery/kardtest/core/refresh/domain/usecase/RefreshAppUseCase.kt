package com.jtuffery.kardtest.core.refresh.domain.usecase

import com.jtuffery.kardtest.core.refresh.domain.manager.RefreshManager

interface RefreshAppUseCase {
    operator fun invoke()
}

fun refreshAppUseCaseFactory(manager: RefreshManager) = object: RefreshAppUseCase {
    override fun invoke() { manager.refresh() }
}