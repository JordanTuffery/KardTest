package com.jtuffery.kardtest.screens.main.domain.repository

import com.jtuffery.kardtest.common.Either

interface UserAvatarRepository {
    suspend fun getAvatar(): Either<Exception, String>
}