package com.jtuffery.kardtest.screens.main.presentation

data class MainUiModel(
    val userAvatarUrl: String?
)
