package com.jtuffery.kardtest.screens.transactiondetails.presentation

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jtuffery.kardtest.common.Either
import com.jtuffery.kardtest.common.UiState
import com.jtuffery.kardtest.core.refresh.domain.usecase.RefreshFlow
import com.jtuffery.kardtest.screens.transactiondetails.domain.usecase.GetTransactionDetailsUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class TransactionDetailsViewModel(
    private val resources: Resources,
    private val getTransactionDetailsUseCase: GetTransactionDetailsUseCase
): ViewModel() {

    private val _uiStateFlow = MutableStateFlow<UiState<TransactionDetailsUiModel>>(UiState.Loading())
    val uiStateFlow = _uiStateFlow

    fun refresh(id: String) {
        viewModelScope.launch {
            uiStateFlow.value = UiState.Loading()
            val result = getTransactionDetailsUseCase(id)
            when(result) {
                is Either.Success ->
                    _uiStateFlow.value = UiState.Success(result.value.toUiModel(resources))
                is Either.Failure ->
                    _uiStateFlow.value = UiState.Error(result.value)
            }
        }
    }
}