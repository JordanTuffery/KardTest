package com.jtuffery.kardtest.screens.transactiondetails.presentation

import android.content.res.Resources
import com.jtuffery.kardtest.R
import com.jtuffery.kardtest.common.toMonthString
import com.jtuffery.kardtest.screens.transactiondetails.domain.model.TransactionDetailsEntity

data class TransactionDetailsUiModel(
    val categoryUrl: String,
    val categoryName: String,
    val title: String,
    val amount: String,
    val avatarUrl: String?,
    val name: String,
    val transactionDate: String,
    val comments: String,
    val linkedImgUrl: String?
)

fun TransactionDetailsEntity.toUiModel(
    resources: Resources
) = with(this) {
    val date =
        "${transactionDate.dayOfMonth} " +
                "${transactionDate.month.toMonthString(resources)} " +
                "${transactionDate.year}"

    TransactionDetailsUiModel(
        categoryUrl = categoryUrl,
        categoryName = categoryName.uppercase(),
        title = title,
        amount = resources.getString(R.string.amount_parsing, amountValue, amountSymbol),
        avatarUrl = avatarUrl,
        name = "$firstName $lastName",
        transactionDate = date,
        comments = resources.getString(R.string.transaction_details_comments, commentsCount),
        linkedImgUrl = linkedImgUrl
    )
}

