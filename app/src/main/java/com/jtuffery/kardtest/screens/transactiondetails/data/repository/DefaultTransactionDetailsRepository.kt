package com.jtuffery.kardtest.screens.transactiondetails.data.repository

import com.jtuffery.kardtest.common.Either
import com.jtuffery.kardtest.screens.transactiondetails.data.datasource.TransactionDetailsDataSource
import com.jtuffery.kardtest.screens.transactiondetails.data.model.toEntity
import com.jtuffery.kardtest.screens.transactiondetails.domain.model.TransactionDetailsEntity
import com.jtuffery.kardtest.screens.transactiondetails.domain.repository.TransactionDetailsRepository

class DefaultTransactionDetailsRepository(
    private val dataSource: TransactionDetailsDataSource
) : TransactionDetailsRepository {
    override suspend fun getTransactionDetails(id: String): Either<Exception, TransactionDetailsEntity> =
        dataSource.getTransactionDetails(id).mapSuccess { it.toEntity() }
}