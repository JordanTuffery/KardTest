package com.jtuffery.kardtest.screens.transactiondetails.domain.usecase

import com.jtuffery.kardtest.common.Either
import com.jtuffery.kardtest.screens.transactiondetails.domain.model.TransactionDetailsEntity
import com.jtuffery.kardtest.screens.transactiondetails.domain.repository.TransactionDetailsRepository

interface GetTransactionDetailsUseCase {
    suspend operator fun invoke(id: String): Either<Exception, TransactionDetailsEntity>
}

fun getTransactionDetailsUseCaseFactory(repository: TransactionDetailsRepository) =
    object: GetTransactionDetailsUseCase {
        override suspend fun invoke(id: String): Either<Exception, TransactionDetailsEntity> =
            repository.getTransactionDetails(id)
    }