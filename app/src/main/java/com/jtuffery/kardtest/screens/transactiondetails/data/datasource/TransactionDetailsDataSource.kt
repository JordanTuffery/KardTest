package com.jtuffery.kardtest.screens.transactiondetails.data.datasource

import com.jtuffery.kardtest.common.Either
import com.jtuffery.kardtest.screens.transactiondetails.data.model.TransactionDetailsDataModel
import com.jtuffery.kardtest.screens.transactiondetails.domain.model.TransactionDetailsEntity

interface TransactionDetailsDataSource {
    suspend fun getTransactionDetails(id: String): Either<Exception, TransactionDetailsDataModel>
}