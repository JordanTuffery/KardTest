package com.jtuffery.kardtest.screens.main.presentation

import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CardGiftcard
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Person
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import coil.transform.CircleCropTransformation
import com.jtuffery.kardtest.R
import com.jtuffery.kardtest.common.UiState
import com.jtuffery.kardtest.theme.KardTheme


@Composable
fun KardBottomBar(
    uiState: UiState<MainUiModel>,
    onItemClicked: (MainViewPagerAdapter.Screen) -> Unit,
    position: Int
) {
    KardTheme {
        Box(
            modifier = Modifier
                .background(
                    Brush.verticalGradient(
                        colors = listOf(
                            Color.Transparent,
                            MaterialTheme.colors.primary.copy(alpha = 0.4f)
                        )
                    )
                )
                .fillMaxWidth()
                .wrapContentHeight()
        ) {
            TabRow(
                selectedTabIndex = position,
                indicator = {
                    KardBottomBarIndicator(
                        it,
                        position
                    )
                },
                divider = {},
                backgroundColor = Color.Transparent,
                contentColor = contentColorFor(backgroundColor = MaterialTheme.colors.primary),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        horizontal = dimensionResource(id = R.dimen.gap_xxxl),
                        vertical = dimensionResource(id = R.dimen.gap_l)
                    )
            ) {
                Icon(
                    imageVector = Icons.Filled.CardGiftcard,
                    contentDescription = "Kardeals",
                    modifier = Modifier
                        .size(65.dp)
                        .clickable { onItemClicked(MainViewPagerAdapter.Screen.KarDeals) }
                        .padding(dimensionResource(id = R.dimen.gap_s))
                )

                Icon(
                    imageVector = Icons.Filled.Home,
                    contentDescription = "Transactions",
                    modifier = Modifier
                        .size(65.dp)
                        .clickable { onItemClicked(MainViewPagerAdapter.Screen.Transactions) }
                        .padding(dimensionResource(id = R.dimen.gap_s))
                )


                if (uiState is UiState.Success && uiState.uim.userAvatarUrl != null) {
                    AvatarItem(
                        avatarUrl = uiState.uim.userAvatarUrl,
                        modifier = Modifier
                            .size(65.dp)
                            .clickable { onItemClicked(MainViewPagerAdapter.Screen.Profile) }
                            .padding(dimensionResource(id = R.dimen.gap_s))
                    )
                } else {
                    Icon(
                        imageVector = Icons.Filled.Person,
                        contentDescription = "Profile",
                        modifier = Modifier
                            .size(65.dp)
                            .clickable { onItemClicked(MainViewPagerAdapter.Screen.Profile) }
                            .padding(dimensionResource(id = R.dimen.gap_s))
                    )
                }
            }
        }
    }
}

@Composable
fun AvatarItem(
    avatarUrl: String,
    modifier: Modifier = Modifier
) {
    Box(
        modifier
            .wrapContentSize(align = Alignment.Center)
            .padding(dimensionResource(id = R.dimen.gap_xs))
            .shadow(1.dp, CircleShape)
            .clip(CircleShape)
            .background(Color.White)
    ) {
        Image(
            painter = rememberImagePainter(
                data = avatarUrl,
                builder = {
                    transformations(CircleCropTransformation())
                }
            ),
            contentDescription = null,
            modifier = Modifier
                .align(Alignment.Center)
                .padding(dimensionResource(id = R.dimen.gap_xxs))
        )
    }
}

@Composable
fun KardBottomBarIndicator(
    tabPositions: List<TabPosition>,
    currentPosition: Int
) {
    val tabPosition = tabPositions[currentPosition]
    val indicatorLeft by animateDpAsState(
        animationSpec = spring(stiffness = Spring.StiffnessMedium),
        targetValue = tabPosition.left
    )
    val indicatorRight = tabPosition.right

    val tabWidth = indicatorRight - indicatorLeft
    val dotWidth = 8.dp
    val dotOffsetX = indicatorLeft + tabWidth / 2 - dotWidth / 2

    Box(
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(align = Alignment.BottomStart)
            .offset(x = dotOffsetX)
            .size(dotWidth)
            .clip(CircleShape)
            .background(contentColorFor(backgroundColor = MaterialTheme.colors.primary))
    )
}

@Preview
@Composable
fun BottomBarPreview() {
    Surface(color = Color.White) {
        KardBottomBar(UiState.Loading(), {}, 0)
    }
}

@Preview
@Composable
fun AvatarItem() {
    KardTheme {
        Surface(color = Color.White) {
            KardBottomBar(UiState.Loading(), {}, 0)
        }
    }
}