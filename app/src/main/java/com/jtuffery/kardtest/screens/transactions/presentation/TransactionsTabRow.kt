package com.jtuffery.kardtest.screens.transactions.presentation

import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateDp
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import com.jtuffery.kardtest.R
import com.jtuffery.kardtest.theme.KardTheme
import com.jtuffery.kardtest.theme.Purple700

enum class TransactionsTab(val titleRes: Int) {
    ME(R.string.transactions_tab_me),
    FRIENDS(R.string.transactions_tab_friends)
}

@Composable
fun TransactionsTabRow(
    selectedItem: TransactionsTab,
    setSelectedItem: (TransactionsTab) -> Unit,
    modifier: Modifier = Modifier
) {
    TabRow(
        selectedTabIndex = selectedItem.ordinal,
        divider = {},
        indicator = {
            TransactionsTabIndicator(
                currentTab = selectedItem,
                tabPositions = it
            )
        },
        backgroundColor = Purple700,
        contentColor = contentColorFor(backgroundColor = MaterialTheme.colors.primary),
        modifier = modifier
            .fillMaxWidth()
            .clip(MaterialTheme.shapes.small)
    ) {
        TransactionsTab.values().forEach {
            TransactionsTabItem(
                isSelected = it == selectedItem,
                tab = it,
                modifier = Modifier
                    .clip(MaterialTheme.shapes.small)
                    .clickable { setSelectedItem(it) }
                    .padding(8.dp)
                    .zIndex(2f)
            )
        }
    }
}

@Composable
fun TransactionsTabIndicator(
    currentTab: TransactionsTab,
    tabPositions: List<TabPosition>
) {
    val transition = updateTransition(
        currentTab,
        label = "Tab indicator"
    )
    val indicatorLeft by transition.animateDp(
        transitionSpec = {
            if (TransactionsTab.ME isTransitioningTo TransactionsTab.FRIENDS) {
                spring(stiffness = Spring.StiffnessLow)
            } else {
                spring(stiffness = Spring.StiffnessMedium)
            }
        },
        label = "Indicator Left"
    ) { page ->
        tabPositions[page.ordinal].left
    }

    val indicatorRight by transition.animateDp(
        transitionSpec = {
            if (TransactionsTab.FRIENDS isTransitioningTo TransactionsTab.ME) {
                spring(stiffness = Spring.StiffnessLow)
            } else {
                spring(stiffness = Spring.StiffnessMedium)
            }
        },
        label = "Indicator Right"
    ) { page ->
        tabPositions[page.ordinal].right
    }

    val padding = 4.dp
    val tabWidth = indicatorRight - indicatorLeft - padding * 2
    val offsetX = indicatorLeft + padding

    Box(
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(align = Alignment.CenterStart)
            .offset(x = offsetX)
            .width(tabWidth)
            .fillMaxHeight(0.75f)
            .shadow(2.dp, MaterialTheme.shapes.small)
            .clip(MaterialTheme.shapes.small)
            .background(MaterialTheme.colors.primary)
            .zIndex(1f)
    )
}

@Composable
fun TransactionsTabItem(
    isSelected: Boolean,
    tab: TransactionsTab,
    modifier: Modifier = Modifier
) {
    val alpha = if (isSelected) ContentAlpha.high else ContentAlpha.disabled
    CompositionLocalProvider(LocalContentAlpha provides alpha) {
        Text(
            text = stringResource(id = tab.titleRes),
            textAlign = TextAlign.Center,
            modifier = modifier
        )
    }
}

@Preview
@Composable
fun TabRowPreview() {
    KardTheme {
        var selectedItem by remember { mutableStateOf(TransactionsTab.ME) }
        TransactionsTabRow(
            selectedItem = selectedItem,
            setSelectedItem = { selectedItem = it }
        )
    }
}