package com.jtuffery.kardtest.screens.transactions.network.data

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.coroutines.await
import com.jtuffery.kardtest.TransactionsQuery
import com.jtuffery.kardtest.common.Either
import com.jtuffery.kardtest.common.flatMap
import com.jtuffery.kardtest.network.launchRequest
import com.jtuffery.kardtest.screens.transactions.data.datasource.TransactionsDataSource
import com.jtuffery.kardtest.screens.transactions.data.model.FriendTransactionDataModel
import com.jtuffery.kardtest.screens.transactions.data.model.TransactionsDataModel
import com.jtuffery.kardtest.screens.transactions.data.model.UserTransactionDataModel
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

class NetworkTransactionsDataSource(
    private val apolloClient: ApolloClient,
    private val timeFormatter: DateTimeFormatter
) : TransactionsDataSource {
    override suspend fun getTransactions(): Either<Exception, TransactionsDataModel> =
        launchRequest {
            apolloClient.query(TransactionsQuery()).await()
        }.flatMap { it.toDataModel() }

    private fun TransactionsQuery.Data.toDataModel(): Either<Exception, TransactionsDataModel> {
        val firstName = me?.profile?.firstName
        val currentBalance = me?.bankAccount?.balance?.value?.toFloat()
        val currencySymbol = me?.bankAccount?.balance?.currency?.symbol
        val userTransactionsList = me?.typedTransactions?.nodes?.toUserTransactionsDataModel()
        val friendsTransactionsList =
            me?.typedFriendsTransactions?.nodes?.toFriendsTransactionsDataModel()

        return if (firstName == null
            || currentBalance == null
            || currencySymbol == null
            || userTransactionsList == null
            || friendsTransactionsList == null
        ) {
            Either.Failure(Exception())
        } else {
            Either.Success(
                TransactionsDataModel(
                    firstName = firstName,
                    currentBalanceValue = currentBalance,
                    currentBalanceSymbol = currencySymbol,
                    userTransactionsList = userTransactionsList,
                    friendsTransactionsList = friendsTransactionsList
                )
            )
        }
    }

    private fun List<TransactionsQuery.Node1?>.toFriendsTransactionsDataModel() = mapNotNull {
        val categoryUrl = it?.category?.image?.url
        val title = it?.title
        val firstName = it?.user?.firstName
        val lastName = it?.user?.lastName
        val avatarUrl = it?.user?.avatar?.url
        val id = it?.id
        val transactionDate =
            it?.createdAt.toString().let { date -> LocalDateTime.parse(date, timeFormatter) }

        if (categoryUrl == null
            || title == null
            || firstName == null
            || lastName == null
            || avatarUrl == null
            || id == null
        ) {
            null
        } else {
            FriendTransactionDataModel(
                categoryUrl = categoryUrl,
                firstName = firstName,
                transactionDate = transactionDate,
                lastName = lastName,
                avatarUrl = avatarUrl,
                title = title,
                id = id,
            )
        }
    }

    private fun List<TransactionsQuery.Node?>.toUserTransactionsDataModel() = mapNotNull {
        val categoryName = it?.category?.name
        val categoryUrl = it?.category?.image?.url
        val title = it?.title
        val id = it?.id
        val amountValue = it?.amount?.value?.toFloat()
        val amountSymbol = it?.amount?.currency?.symbol
        val transactionDate =
            it?.createdAt.toString().let { date -> LocalDateTime.parse(date, timeFormatter) }

        if (categoryName == null
            || categoryUrl == null
            || title == null
            || amountValue == null
            || amountSymbol == null
            || id == null
        ) {
            null
        } else {
            UserTransactionDataModel(
                categoryUrl = categoryUrl,
                categoryName = categoryName,
                transactionDate = transactionDate,
                title = title,
                id = id,
                amountValue = amountValue,
                amountSymbol = amountSymbol
            )
        }
    }
}