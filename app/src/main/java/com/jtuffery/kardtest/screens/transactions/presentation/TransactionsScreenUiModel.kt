package com.jtuffery.kardtest.screens.transactions.presentation

import android.content.res.Resources
import com.jtuffery.kardtest.R
import com.jtuffery.kardtest.common.*
import com.jtuffery.kardtest.screens.transactions.domain.model.FriendTransactionEntity
import com.jtuffery.kardtest.screens.transactions.domain.model.TransactionsEntity
import com.jtuffery.kardtest.screens.transactions.domain.model.UserTransactionEntity
import org.threeten.bp.LocalDateTime

data class TransactionsScreenUiModel(
    val nameCatchPhrase: String,
    val currentBalance: String,
    val onClickAdd: () -> Unit,
    val onClickSend: () -> Unit,
    val onClickAddGooglePay: () -> Unit,
    val userTransactionsList: List<TransactionsListUiModel>,
    val friendTransactionsList: List<TransactionsListUiModel>,
)

data class TransactionsListUiModel(
    val title: String,
    val amountSummary: String?,
    val transactionsList: List<TransactionUiModel>
)

sealed class TransactionUiModel(
    open val title: String,
    open val id: String
) {
    data class UserTransactionUiModel(
        val categoryUrl: String,
        val categoryName: String,
        val amount: String,
        override val title: String,
        override val id: String,
        val onItemClick: (String) -> Unit
    ) : TransactionUiModel(title, id)

    data class FriendTransactionUiModel(
        val categoryUrl: String,
        val avatarUrl: String,
        val name: String,
        override val title: String,
        override val id: String
    ) : TransactionUiModel(title, id)
}

fun TransactionsEntity.toUiModel(
    resources: Resources,
    onClickAdd: () -> Unit,
    onClickSend: () -> Unit,
    onClickAddGooglePay: () -> Unit,
    onItemClick: (String) -> Unit
) = TransactionsScreenUiModel(
    nameCatchPhrase = resources.getString(R.string.transactions_name_catchphrase, firstName),
    currentBalance = "$currentBalanceValue$currentBalanceSymbol",
    onClickAdd = onClickAdd,
    onClickSend = onClickSend,
    onClickAddGooglePay = onClickAddGooglePay,
    userTransactionsList = userTransactionsList.toUiModel(resources, onItemClick),
    friendTransactionsList = friendsTransactionsList.toUiModel(resources)
)

enum class TransactionSection(
    val titleRes: Int,
    val predicate: (LocalDateTime) -> Boolean
) {
    TODAY(R.string.today, { it.isToday() }),
    YESTERDAY(R.string.yesterday, { it.isYesterday() }),
    LASTWEEK(R.string.last_week, { it.isLastWeek() }),
    LAST2WEEK(R.string.last_two_week, { it.isLast2Week() }),
    REMAININGMONTH(R.string.remaining_month, { it.isRemainingThisMonth() }),
    LASTMONTH(R.string.last_month, { it.isLastMonth() }),
    BEFORE(R.string.before_last_month, { it.isBeforeLastMonth() })
}

fun List<UserTransactionEntity>.toTransactionUiModel(
    resources: Resources,
    section: TransactionSection,
    onItemClick: (String) -> Unit
): TransactionsListUiModel {
    val transactions = filter { section.predicate(it.transactionDate) }
    return TransactionsListUiModel(
        resources.getString(section.titleRes),
        resources.getString(
            R.string.amount_parsing,
            transactions.sumOf { it.amountValue.toDouble() },
            transactions.firstOrNull()?.amountSymbol ?: ""
        ),
        transactions.map { it.toUiModel(resources, onItemClick) }
    )
}

fun List<FriendTransactionEntity>.toTransactionUiModel(
    resources: Resources,
    section: TransactionSection
): TransactionsListUiModel {
    val transactions = filter { section.predicate(it.transactionDate) }
    return TransactionsListUiModel(
        resources.getString(section.titleRes),
        null,
        transactions.map { it.toUiModel() }
    )
}

fun List<UserTransactionEntity>.toUiModel(
    resources: Resources,
    onItemClick: (String) -> Unit
) = arrayListOf<TransactionsListUiModel>().also {
    it.add(toTransactionUiModel(resources, TransactionSection.TODAY, onItemClick))
    it.add(toTransactionUiModel(resources, TransactionSection.YESTERDAY, onItemClick))
    it.add(toTransactionUiModel(resources, TransactionSection.LASTWEEK, onItemClick))
    it.add(toTransactionUiModel(resources, TransactionSection.LAST2WEEK, onItemClick))
    it.add(toTransactionUiModel(resources, TransactionSection.REMAININGMONTH, onItemClick))
    it.add(toTransactionUiModel(resources, TransactionSection.LASTMONTH, onItemClick))
    it.add(toTransactionUiModel(resources, TransactionSection.BEFORE, onItemClick))
}.filter { it.transactionsList.isNotEmpty() }


fun List<FriendTransactionEntity>.toUiModel(
    resources: Resources
) = arrayListOf<TransactionsListUiModel>().also {
    it.add(toTransactionUiModel(resources, TransactionSection.TODAY))
    it.add(toTransactionUiModel(resources, TransactionSection.YESTERDAY))
    it.add(toTransactionUiModel(resources, TransactionSection.LASTWEEK))
    it.add(toTransactionUiModel(resources, TransactionSection.LAST2WEEK))
    it.add(toTransactionUiModel(resources, TransactionSection.REMAININGMONTH))
    it.add(toTransactionUiModel(resources, TransactionSection.LASTMONTH))
    it.add(toTransactionUiModel(resources, TransactionSection.BEFORE))
}.filter { it.transactionsList.isNotEmpty() }

fun UserTransactionEntity.toUiModel(resources: Resources, onItemClick: (String) -> Unit) =
    TransactionUiModel.UserTransactionUiModel(
        categoryName = categoryName,
        categoryUrl = categoryUrl,
        title = title,
        id = id,
        amount = resources.getString(R.string.amount_parsing, amountValue, amountSymbol),
        onItemClick = onItemClick
    )

fun FriendTransactionEntity.toUiModel() =
    TransactionUiModel.FriendTransactionUiModel(
        categoryUrl = categoryUrl,
        title = title,
        name = "$firstName $lastName",
        avatarUrl = avatarUrl,
        id = id,
    )

internal fun generateMockData() = TransactionsScreenUiModel(
    nameCatchPhrase = "Salut Basile \uD83D\uDC4B",
    currentBalance = "1686.54€",
    onClickAdd = {},
    onClickSend = {},
    onClickAddGooglePay = {},
    userTransactionsList = listOf(
        TransactionsListUiModel(
            "Aujourd'hui",
            "-256.32",
            listOf(
                TransactionUiModel.UserTransactionUiModel(
                    "https://staging.kard.eu/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt4TkRZM056bGlaQzFqT1dVNUxUUTRNVEl0WWpZNVpTMW1aR1F3TkRjM05UZzBaV1lHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6ImJsb2JfaWQifX0=--bae9d147d070ad22ab0d9d2f7183fd2d88b4ad28/Ico-Top%20Up.png",
                    "Petit",
                    "-5.00€",
                    "Transaction 1",
                    "id",
                    {}
                ),
                TransactionUiModel.UserTransactionUiModel(
                    "https://staging.kard.eu/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt4TkRZM056bGlaQzFqT1dVNUxUUTRNVEl0WWpZNVpTMW1aR1F3TkRjM05UZzBaV1lHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6ImJsb2JfaWQifX0=--bae9d147d070ad22ab0d9d2f7183fd2d88b4ad28/Ico-Top%20Up.png",
                    "Moyen",
                    "15.00€",
                    "Transaction 2",
                    "id",
                    {}
                ),
                TransactionUiModel.UserTransactionUiModel(
                    "https://staging.kard.eu/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt4TkRZM056bGlaQzFqT1dVNUxUUTRNVEl0WWpZNVpTMW1aR1F3TkRjM05UZzBaV1lHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6ImJsb2JfaWQifX0=--bae9d147d070ad22ab0d9d2f7183fd2d88b4ad28/Ico-Top%20Up.png",
                    "Grand",
                    "25.00€",
                    "Transaction 3",
                    "id",
                    {}
                ),
                TransactionUiModel.UserTransactionUiModel(
                    "https://staging.kard.eu/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt4TkRZM056bGlaQzFqT1dVNUxUUTRNVEl0WWpZNVpTMW1aR1F3TkRjM05UZzBaV1lHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6ImJsb2JfaWQifX0=--bae9d147d070ad22ab0d9d2f7183fd2d88b4ad28/Ico-Top%20Up.png",
                    "Petit",
                    "-5.00€",
                    "Transaction 4",
                    "id",
                    {}
                ),
                TransactionUiModel.UserTransactionUiModel(
                    "https://staging.kard.eu/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt4TkRZM056bGlaQzFqT1dVNUxUUTRNVEl0WWpZNVpTMW1aR1F3TkRjM05UZzBaV1lHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6ImJsb2JfaWQifX0=--bae9d147d070ad22ab0d9d2f7183fd2d88b4ad28/Ico-Top%20Up.png",
                    "Moyen",
                    "15.00€",
                    "Transaction 5",
                    "id",
                    {}
                ),
                TransactionUiModel.UserTransactionUiModel(
                    "https://staging.kard.eu/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt4TkRZM056bGlaQzFqT1dVNUxUUTRNVEl0WWpZNVpTMW1aR1F3TkRjM05UZzBaV1lHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6ImJsb2JfaWQifX0=--bae9d147d070ad22ab0d9d2f7183fd2d88b4ad28/Ico-Top%20Up.png",
                    "Grand",
                    "-25.00€",
                    "Transaction 6",
                    "id",
                    {}
                ),
                TransactionUiModel.UserTransactionUiModel(
                    "https://staging.kard.eu/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt4TkRZM056bGlaQzFqT1dVNUxUUTRNVEl0WWpZNVpTMW1aR1F3TkRjM05UZzBaV1lHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6ImJsb2JfaWQifX0=--bae9d147d070ad22ab0d9d2f7183fd2d88b4ad28/Ico-Top%20Up.png",
                    "Petit",
                    "5.00€",
                    "Transaction 7",
                    "id",
                    {}
                ),
                TransactionUiModel.UserTransactionUiModel(
                    "https://staging.kard.eu/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt4TkRZM056bGlaQzFqT1dVNUxUUTRNVEl0WWpZNVpTMW1aR1F3TkRjM05UZzBaV1lHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6ImJsb2JfaWQifX0=--bae9d147d070ad22ab0d9d2f7183fd2d88b4ad28/Ico-Top%20Up.png",
                    "Moyen",
                    "15.00€",
                    "Transaction 8",
                    "id",
                    {}
                ),
                TransactionUiModel.UserTransactionUiModel(
                    "https://staging.kard.eu/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt4TkRZM056bGlaQzFqT1dVNUxUUTRNVEl0WWpZNVpTMW1aR1F3TkRjM05UZzBaV1lHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6ImJsb2JfaWQifX0=--bae9d147d070ad22ab0d9d2f7183fd2d88b4ad28/Ico-Top%20Up.png",
                    "Grand",
                    "25.00€",
                    "Transaction 9",
                    "id",
                    {}
                ),
                TransactionUiModel.UserTransactionUiModel(
                    "https://staging.kard.eu/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt4TkRZM056bGlaQzFqT1dVNUxUUTRNVEl0WWpZNVpTMW1aR1F3TkRjM05UZzBaV1lHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6ImJsb2JfaWQifX0=--bae9d147d070ad22ab0d9d2f7183fd2d88b4ad28/Ico-Top%20Up.png",
                    "Petit",
                    "-5.00€",
                    "Transaction 10",
                    "id",
                    {}
                ),
                TransactionUiModel.UserTransactionUiModel(
                    "https://staging.kard.eu/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt4TkRZM056bGlaQzFqT1dVNUxUUTRNVEl0WWpZNVpTMW1aR1F3TkRjM05UZzBaV1lHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6ImJsb2JfaWQifX0=--bae9d147d070ad22ab0d9d2f7183fd2d88b4ad28/Ico-Top%20Up.png",
                    "Moyen",
                    "-15.00€",
                    "Transaction 11",
                    "id",
                    {}
                ),
                TransactionUiModel.UserTransactionUiModel(
                    "https://staging.kard.eu/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt4TkRZM056bGlaQzFqT1dVNUxUUTRNVEl0WWpZNVpTMW1aR1F3TkRjM05UZzBaV1lHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6ImJsb2JfaWQifX0=--bae9d147d070ad22ab0d9d2f7183fd2d88b4ad28/Ico-Top%20Up.png",
                    "Grand",
                    "25.00€",
                    "Transaction 12",
                    "id",
                    {}
                ),
            ),
        )
    ),
    emptyList()
)


