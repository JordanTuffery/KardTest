package com.jtuffery.kardtest.screens.main.data.datasource

import com.jtuffery.kardtest.common.Either

interface UserAvatarDataSource {
    suspend fun getAvatar(): Either<Exception, String>
}