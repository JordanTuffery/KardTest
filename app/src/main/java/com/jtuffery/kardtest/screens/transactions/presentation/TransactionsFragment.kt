package com.jtuffery.kardtest.screens.transactions.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import com.jtuffery.kardtest.common.UiState
import com.jtuffery.kardtest.common.observe
import com.jtuffery.kardtest.core.refresh.domain.usecase.RefreshFlow
import com.jtuffery.kardtest.core.refresh.presentation.RefreshViewModel
import com.jtuffery.kardtest.screens.transactiondetails.presentation.TransactionDetailsBottomDialogFragment
import com.jtuffery.kardtest.theme.KardTheme
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class TransactionsFragment : Fragment() {
    private val transactionsViewModel: TransactionsViewModel by viewModel()
    private val refreshViewModel: RefreshViewModel by sharedViewModel()
    private val refreshFlow: RefreshFlow by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                val uiState: UiState<TransactionsScreenUiModel> by transactionsViewModel.uiStateFlow.collectAsState()
                KardTheme {
                    TransactionsScreen(uiState, refreshViewModel::refresh)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        transactionsViewModel.actionsFlow.observe(viewLifecycleOwner) {
            when(it) {
                is TransactionsViewModel.Action.OpenDetails -> {
                    val dialog = TransactionDetailsBottomDialogFragment.newInstance(it.id)
                    dialog.show(
                        childFragmentManager,
                        "${TransactionDetailsBottomDialogFragment::class.java.simpleName}${it.id}"
                    )
                }
            }
        }

        refreshFlow.observe(viewLifecycleOwner) {
            transactionsViewModel.refresh()
        }
    }

    override fun onStart() {
        super.onStart()
        transactionsViewModel.refresh()
    }
}
