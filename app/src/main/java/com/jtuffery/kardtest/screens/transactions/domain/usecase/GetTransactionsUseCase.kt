package com.jtuffery.kardtest.screens.transactions.domain.usecase

import com.jtuffery.kardtest.common.Either
import com.jtuffery.kardtest.screens.transactions.domain.model.TransactionsEntity
import com.jtuffery.kardtest.screens.transactions.domain.repository.TransactionsRepository

interface GetTransactionsUseCase {
    suspend operator fun invoke(): Either<Exception, TransactionsEntity>
}

fun getTransactionsUseCaseFactory(repository: TransactionsRepository) =
    object: GetTransactionsUseCase {
        override suspend operator fun invoke(): Either<Exception, TransactionsEntity> =
            repository.getTransactions()
    }