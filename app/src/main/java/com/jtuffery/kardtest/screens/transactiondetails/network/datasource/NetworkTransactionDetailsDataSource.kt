package com.jtuffery.kardtest.screens.transactiondetails.network.datasource

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.coroutines.await
import com.jtuffery.kardtest.GetTransactionQuery
import com.jtuffery.kardtest.common.Either
import com.jtuffery.kardtest.common.flatMap
import com.jtuffery.kardtest.network.launchRequest
import com.jtuffery.kardtest.screens.transactiondetails.data.datasource.TransactionDetailsDataSource
import com.jtuffery.kardtest.screens.transactiondetails.data.model.TransactionDetailsDataModel
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import timber.log.Timber

class NetworkTransactionDetailsDataSource(
    private val apolloClient: ApolloClient,
    private val timeFormatter: DateTimeFormatter
) : TransactionDetailsDataSource {
    override suspend fun getTransactionDetails(id: String): Either<Exception, TransactionDetailsDataModel> =
        launchRequest { apolloClient.query(GetTransactionQuery(id)).await() }
            .flatMap { it.toDataModel() }

    private fun GetTransactionQuery.Data.toDataModel(): Either<Exception, TransactionDetailsDataModel> {
        return try {
            val categoryUrl = transaction?.category?.image?.url
            val categoryName = transaction?.category?.name
            val title = transaction?.title
            val amountValue = transaction?.amount?.value?.toFloat()
            val amountSymbol = transaction?.amount?.currency?.symbol
            val avatarUrl = transaction?.user?.avatar?.url
            val firstName = transaction?.user?.firstName
            val lastName = transaction?.user?.lastName
            val transactionDate = transaction?.createdAt.toString().let { LocalDateTime.parse(it, timeFormatter) }
            val commentsCount = transaction?.comments?.totalCount
            val linkedImgUrl = transaction?.image?.url

             if (categoryUrl == null ||
                categoryName == null ||
                title == null ||
                amountValue == null ||
                amountSymbol == null ||
                firstName == null ||
                lastName == null ||
                commentsCount == null
            ) {
                Either.Failure(Exception())
            } else {
                Either.Success(
                    TransactionDetailsDataModel(
                        categoryUrl = categoryUrl,
                        categoryName = categoryName,
                        title = title,
                        amountValue = amountValue,
                        amountSymbol = amountSymbol,
                        avatarUrl = avatarUrl,
                        firstName = firstName,
                        lastName = lastName,
                        transactionDate = transactionDate,
                        commentsCount = commentsCount,
                        linkedImgUrl = linkedImgUrl
                    )
                )
            }
        } catch (e: Exception) {
            Timber.e(e)
            Either.Failure(e)
        }
    }
}