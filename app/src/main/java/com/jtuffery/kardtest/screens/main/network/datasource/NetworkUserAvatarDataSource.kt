package com.jtuffery.kardtest.screens.main.network.datasource

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.coroutines.await
import com.jtuffery.kardtest.GetUserAvatarQuery
import com.jtuffery.kardtest.common.Either
import com.jtuffery.kardtest.common.flatMap
import com.jtuffery.kardtest.network.launchRequest
import com.jtuffery.kardtest.screens.main.data.datasource.UserAvatarDataSource

class NetworkUserAvatarDataSource(
    private val apolloClient: ApolloClient
): UserAvatarDataSource {
    override suspend fun getAvatar(): Either<Exception, String> =
        launchRequest { apolloClient.query(GetUserAvatarQuery()).await() }
            .flatMap {
                val url = it.me?.profile?.avatar?.url
                if(url == null) {
                    Either.Failure(Exception())
                } else {
                    Either.Success(url)
                }
            }
}