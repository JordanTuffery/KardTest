package com.jtuffery.kardtest.screens.main.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.viewpager2.widget.ViewPager2
import com.jtuffery.kardtest.R
import com.jtuffery.kardtest.common.UiState
import com.jtuffery.kardtest.common.observe
import com.jtuffery.kardtest.core.refresh.domain.usecase.RefreshFlow
import com.jtuffery.kardtest.databinding.ActivityMainBinding
import kotlinx.coroutines.flow.MutableStateFlow
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val mainViewModel: MainViewModel by viewModel()
    private val refreshFlow: RefreshFlow by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.statusBarColor = ResourcesCompat.getColor(resources, R.color.purple_700, null)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(
            this, R.layout.activity_main
        )

        val positionFlow = MutableStateFlow(0)
        binding.viewpager2Main.apply {
            adapter = MainViewPagerAdapter(supportFragmentManager, lifecycle)
            registerOnPageChangeCallback(
                object: ViewPager2.OnPageChangeCallback() {
                    override fun onPageSelected(position: Int) {
                        super.onPageSelected(position)
                        positionFlow.value = position
                    }
                }
            )
            setCurrentItem(MainViewPagerAdapter.Screen.Transactions.ordinal, false)
            offscreenPageLimit = 2
        }
        binding.composeviewMainBottombar.setContent {
            val position: Int by positionFlow.collectAsState()
            val uiState: UiState<MainUiModel> by mainViewModel.uim.collectAsState()

            KardBottomBar(
                uiState = uiState,
                onItemClicked = {
                    binding.viewpager2Main.setCurrentItem(it.ordinal, true)
                },
                position = position
            )
        }

        refreshFlow.observe(this) {
            mainViewModel.refreshAvatar()
        }
    }

    override fun onStart() {
        super.onStart()
        mainViewModel.refreshAvatar()
    }
}