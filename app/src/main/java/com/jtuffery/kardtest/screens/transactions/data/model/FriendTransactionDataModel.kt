package com.jtuffery.kardtest.screens.transactions.data.model

import com.jtuffery.kardtest.screens.transactions.domain.model.FriendTransactionEntity

typealias FriendTransactionDataModel = FriendTransactionEntity

fun FriendTransactionDataModel.toEntity(): FriendTransactionEntity = this