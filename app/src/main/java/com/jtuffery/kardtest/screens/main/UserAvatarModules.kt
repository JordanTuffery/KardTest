package com.jtuffery.kardtest.screens.main

import com.jtuffery.kardtest.screens.main.data.datasource.UserAvatarDataSource
import com.jtuffery.kardtest.screens.main.data.repository.DefaultUserAvatarRepository
import com.jtuffery.kardtest.screens.main.domain.repository.UserAvatarRepository
import com.jtuffery.kardtest.screens.main.domain.usecase.getUserAvatarUseCaseFactory
import com.jtuffery.kardtest.screens.main.network.datasource.NetworkUserAvatarDataSource
import org.koin.dsl.module

val userAvatarNetworkModule = module {
    single<UserAvatarDataSource> { NetworkUserAvatarDataSource(get()) }
}

val userAvatarDataModule = module {
    single<UserAvatarRepository> { DefaultUserAvatarRepository(get()) }
}

val userAvatarDomainModule = module {
    factory { getUserAvatarUseCaseFactory(get()) }
}

val userAvatarCoreModules = userAvatarNetworkModule +
        userAvatarDataModule +
        userAvatarDomainModule