package com.jtuffery.kardtest.screens.transactiondetails

import android.content.Context
import com.jtuffery.kardtest.screens.main.DATE_TIME_FORMATTER_ISO8601
import com.jtuffery.kardtest.screens.transactiondetails.data.datasource.TransactionDetailsDataSource
import com.jtuffery.kardtest.screens.transactiondetails.data.repository.DefaultTransactionDetailsRepository
import com.jtuffery.kardtest.screens.transactiondetails.domain.repository.TransactionDetailsRepository
import com.jtuffery.kardtest.screens.transactiondetails.domain.usecase.getTransactionDetailsUseCaseFactory
import com.jtuffery.kardtest.screens.transactiondetails.network.datasource.NetworkTransactionDetailsDataSource
import com.jtuffery.kardtest.screens.transactiondetails.presentation.TransactionDetailsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val transactionDetailsNetworkModule = module {
    single<TransactionDetailsDataSource> { NetworkTransactionDetailsDataSource(get(), get(named(DATE_TIME_FORMATTER_ISO8601))) }
}

val transactionDetailsDataModule = module {
    single<TransactionDetailsRepository> { DefaultTransactionDetailsRepository(get()) }
}

val transactionDetailsDomainModule = module {
    factory { getTransactionDetailsUseCaseFactory(get()) }
}

val transactionDetailsPresentationModule = module {
    viewModel { TransactionDetailsViewModel(get<Context>().resources, get()) }
}

val transactionDetailsModules = transactionDetailsNetworkModule +
        transactionDetailsDataModule +
        transactionDetailsDomainModule +
        transactionDetailsPresentationModule