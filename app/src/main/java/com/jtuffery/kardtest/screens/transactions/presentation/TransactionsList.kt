package com.jtuffery.kardtest.screens.transactions.presentation

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ChevronRight
import androidx.compose.material.icons.rounded.ChevronRight
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import coil.transform.CircleCropTransformation
import com.jtuffery.kardtest.R
import com.jtuffery.kardtest.theme.Green200
import com.jtuffery.kardtest.theme.KardTheme

@Composable
fun TransactionsList(uiModel: TransactionsListUiModel, modifier: Modifier = Modifier) {
    Column(modifier) {
        TransactionsListHeader(uiModel, Modifier.padding(horizontal = dimensionResource(id = R.dimen.gap_m)))
        TransactionsListItem(uiModel, Modifier.padding(top = dimensionResource(id = R.dimen.gap_s)))
    }
}

@Composable
fun TransactionsListHeader(uiModel: TransactionsListUiModel, modifier: Modifier = Modifier) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = modifier.fillMaxWidth()
    ) {
        Text(
            text = uiModel.title.uppercase(),
            style = MaterialTheme.typography.body2
        )
        uiModel.amountSummary?.let {

            CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                TransactionAnnotatedAmount(
                    amount = it,
                    tallStyle = MaterialTheme.typography.body1.copy(fontSize = 13.sp),
                    smallStyle = MaterialTheme.typography.body2.copy(fontSize = 10.sp)
                )
            }
        }
    }
}

@Composable
fun TransactionsListItem(uiModel: TransactionsListUiModel, modifier: Modifier = Modifier) {
    Surface(
        color = MaterialTheme.colors.surface,
        contentColor = MaterialTheme.colors.onSurface,
        modifier = modifier
            .shadow(dimensionResource(id = R.dimen.gap_xs), MaterialTheme.shapes.large)
            .clip(MaterialTheme.shapes.large)
    ) {
        Column {
            uiModel.transactionsList.forEachIndexed { index, item ->
                when (item) {
                    is TransactionUiModel.FriendTransactionUiModel ->
                        FriendTransactionItem(uiModel = item)
                    is TransactionUiModel.UserTransactionUiModel ->
                        UserTransactionItem(uiModel = item)
                }

                if (index < uiModel.transactionsList.size - 1)
                    Divider(
                        color = MaterialTheme.colors.onSurface.copy(alpha = 0.05f),
                        thickness = 1.dp,
                        modifier = Modifier.padding(horizontal = dimensionResource(id = R.dimen.gap_m))
                    )
            }
        }
    }
}

@Composable
fun UserTransactionItem(uiModel: TransactionUiModel.UserTransactionUiModel) {
    TransactionItem(
        title = uiModel.title,
        subtitle = uiModel.categoryName,
        onClickItem = { uiModel.onItemClick(uiModel.id) },
        Start = { modifier ->
            TransactionIcon(
                uiModel.categoryUrl,
                modifier
                    .padding(dimensionResource(id = R.dimen.gap_m))
                    .size(50.dp)
            )
        },
        End = { modifier ->
            val textTypo = if (uiModel.amount.contains("-")) {
                MaterialTheme.typography.h6
            } else {
                MaterialTheme.typography.h6.copy(color = Green200)
            }
            Text(
                text = uiModel.amount,
                style = textTypo,
                modifier = modifier.padding(end = dimensionResource(id = R.dimen.gap_m))
            )
        }
    )
}

@Composable
fun FriendTransactionItem(uiModel: TransactionUiModel.FriendTransactionUiModel) {
    TransactionItem(
        title = uiModel.title,
        subtitle = uiModel.name,
        onClickItem = {},
        Start = { modifier ->
            Box(
                contentAlignment = Alignment.BottomEnd,
                modifier = Modifier.padding(dimensionResource(id = R.dimen.gap_m))
            ) {
                TransactionIcon(
                    uiModel.avatarUrl,
                    modifier
                        .align(Alignment.Center)
                        .size(50.dp)
                )
                CategoryIcon(
                    uiModel.categoryUrl,
                    modifier
                        .align(Alignment.BottomCenter)
                        .size(25.dp)
                )
            }
        },
        End = { modifier ->
            CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.disabled) {
                Icon(
                    imageVector = Icons.Rounded.ChevronRight,
                    contentDescription = "Chevron Right",
                    modifier = modifier
                        .size(50.dp)
                        .padding(end = dimensionResource(id = R.dimen.gap_m))
                )
            }
        }
    )
}

@Composable
fun TransactionItem(
    title: String,
    subtitle: String,
    onClickItem: () -> Unit,
    Start: @Composable (Modifier) -> Unit,
    End: @Composable (Modifier) -> Unit,
    modifier: Modifier = Modifier
) {
    Row(
        modifier
            .fillMaxWidth()
            .clickable { onClickItem() }
    ) {
        Start(Modifier.align(Alignment.CenterVertically))

        Column(
            Modifier
                .padding(vertical = dimensionResource(id = R.dimen.gap_m))
                .align(Alignment.CenterVertically)
                .weight(1f)
        ) {
            Text(text = title, style = MaterialTheme.typography.body1)
            CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                Text(text = subtitle, style = MaterialTheme.typography.body2)
            }
        }

        End(Modifier.align(Alignment.CenterVertically))
    }
}

@Composable
fun TransactionIcon(
    imageUrl: String,
    modifier: Modifier = Modifier
) {
    Box(
        modifier
            .clip(CircleShape)
    ) {
        Image(
            painter = rememberImagePainter(
                data = imageUrl,
                builder = {
                    transformations(CircleCropTransformation())
                }
            ),
            contentDescription = null,
            modifier = Modifier.align(Alignment.Center)
        )
    }
}

@Composable
fun CategoryIcon(
    imageUrl: String,
    modifier: Modifier = Modifier
) {
    Box(
        modifier
            .clip(CircleShape)
            .background(Color.White)
    ) {
        Image(
            painter = rememberImagePainter(
                data = imageUrl,
                builder = {
                    transformations(CircleCropTransformation())
                }
            ),
            contentDescription = null,
            modifier = Modifier.padding(1.dp)
        )
    }
}

@Preview
@Composable
fun TransactionsListPreview() {
    KardTheme {
        Surface(color = MaterialTheme.colors.primary) {
            TransactionsList(uiModel = generateMockData().userTransactionsList.first())
        }
    }
}