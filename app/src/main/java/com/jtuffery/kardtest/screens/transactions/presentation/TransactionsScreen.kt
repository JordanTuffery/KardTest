package com.jtuffery.kardtest.screens.transactions.presentation

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material.icons.rounded.ArrowForward
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.jtuffery.kardtest.R
import com.jtuffery.kardtest.common.UiState
import com.jtuffery.kardtest.designsystem.ChipButton
import com.jtuffery.kardtest.designsystem.ErrorComposable
import com.jtuffery.kardtest.designsystem.LoaderComposable
import com.jtuffery.kardtest.theme.Blue200
import com.jtuffery.kardtest.theme.DarkPurple1
import com.jtuffery.kardtest.theme.Green200
import com.jtuffery.kardtest.theme.KardTheme

@Composable
fun TransactionsScreen(
    uiState: UiState<TransactionsScreenUiModel>,
    onRetryClick: () -> Unit
) {
    Scaffold(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth()
    ) {
        Surface(
            color = MaterialTheme.colors.primary,
            contentColor = contentColorFor(backgroundColor = MaterialTheme.colors.primary),
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
        ) {
            when (uiState) {
                is UiState.Loading -> LoaderComposable()
                is UiState.Error -> ErrorComposable(onRetryClick)
                is UiState.Success -> {
                    val uim = uiState.uim
                    var selectedItem by remember { mutableStateOf(TransactionsTab.ME) }
                    LazyColumn {
                        item {
                            TransactionsHeader(
                                uim,
                                selectedItem,
                                { selectedItem = it },
                                Modifier
                                    .padding(horizontal = dimensionResource(id = R.dimen.gap_m))
                                    .padding(top = dimensionResource(id = R.dimen.gap_m))
                            )
                        }
                        item {
                            when (selectedItem) {
                                TransactionsTab.ME -> uim.userTransactionsList
                                TransactionsTab.FRIENDS -> uim.friendTransactionsList
                            }.forEach { transactionListUiModel ->
                                TransactionsList(
                                    transactionListUiModel,
                                    Modifier.padding(dimensionResource(id = R.dimen.gap_s))
                                )
                            }
                        }
                        item {
                            Box(
                                modifier = Modifier
                                    .background(Color.Transparent)
                                    .padding(50.dp)
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun TransactionAnnotatedAmount(
    amount: String,
    tallStyle: TextStyle,
    smallStyle: TextStyle,
    modifier: Modifier = Modifier
) {
    Text(
        text = buildAnnotatedString {
            val currentBalance = amount.split(',', '.')
            withStyle(style = tallStyle.toSpanStyle()) {
                append(currentBalance[0])
            }
            withStyle(style = smallStyle.toSpanStyle()) {
                append(",${currentBalance[1]}")
            }
        },
        modifier = modifier
    )
}

@Composable
fun TransactionsHeader(
    uim: TransactionsScreenUiModel,
    selectedItem: TransactionsTab,
    setSelectedItem: (TransactionsTab) -> Unit,
    modifier: Modifier = Modifier
) {
    Column(modifier) {
        Text(
            text = uim.nameCatchPhrase,
            style = MaterialTheme.typography.h3,
            modifier = Modifier.padding(top = dimensionResource(id = R.dimen.gap_m))
        )
        CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.disabled) {
            Text(
                text = stringResource(id = R.string.transactions_current_balance_title),
                style = MaterialTheme.typography.subtitle2,
                modifier = Modifier.padding(top = dimensionResource(id = R.dimen.gap_m))
            )
        }

        TransactionAnnotatedAmount(
            amount = uim.currentBalance,
            tallStyle = MaterialTheme.typography.h3,
            smallStyle = MaterialTheme.typography.h4,
            modifier = Modifier.padding(top = dimensionResource(id = R.dimen.gap_s))
        )

        Row(
            Modifier.padding(top = dimensionResource(id = R.dimen.gap_s))
        ) {
            ChipButton(
                Icons.Rounded.Add,
                "add",
                Green200,
                stringResource(id = R.string.transactions_add_button),
                uim.onClickAdd,
                Modifier.padding(end = 8.dp)
            )
            ChipButton(
                Icons.Rounded.ArrowForward,
                "send",
                Blue200,
                stringResource(id = R.string.transactions_send_button),
                uim.onClickSend
            )
        }

        AddGooglePayButton(
            onClick = uim.onClickAddGooglePay,
            modifier = Modifier.padding(top = dimensionResource(id = R.dimen.gap_m))
        )

        TransactionsTabRow(
            selectedItem,
            setSelectedItem,
            Modifier.padding(top = dimensionResource(id = R.dimen.gap_m))
        )
    }
}

@Composable
fun AddGooglePayButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Surface(
        color = DarkPurple1,
        contentColor = contentColorFor(backgroundColor = DarkPurple1),
        modifier = modifier
            .fillMaxWidth()
            .shadow(dimensionResource(id = R.dimen.gap_xs), MaterialTheme.shapes.medium)
            .clip(MaterialTheme.shapes.medium)
            .clickable { onClick() }
    ) {
        Row(
            modifier = Modifier.padding(
                start = dimensionResource(id = R.dimen.gap_s),
                end = dimensionResource(id = R.dimen.gap_s),
                top = dimensionResource(id = R.dimen.gap_m),
                bottom = dimensionResource(id = R.dimen.gap_m)
            )
        ) {
            Column(Modifier.padding(start = dimensionResource(id = R.dimen.gap_l))) {
                Text(
                    text = stringResource(id = R.string.transactions_add_google_pay_title),
                    style = MaterialTheme.typography.body1
                )
                CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.disabled) {
                    Text(
                        text = stringResource(id = R.string.transactions_add_google_pay_subtitle),
                        style = MaterialTheme.typography.body2,
                        modifier = Modifier.padding(top = dimensionResource(id = R.dimen.gap_xs))
                    )
                }
            }
            Image(
                painter = painterResource(id = R.drawable.ic_google_pay),
                contentDescription = "Google pay"
            )
        }
    }
}

@Preview("LightTheme")
@Composable
fun TransactionScreenPreviewLight() {
    KardTheme {
        TransactionsScreen(
            UiState.Success(
                generateMockData()
            ),
            {}
        )
    }
}
