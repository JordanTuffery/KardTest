package com.jtuffery.kardtest.screens.transactions

import android.content.Context
import com.jtuffery.kardtest.screens.main.DATE_TIME_FORMATTER_ISO8601
import com.jtuffery.kardtest.screens.transactions.data.datasource.TransactionsDataSource
import com.jtuffery.kardtest.screens.transactions.data.repository.DefaultTransactionsRepository
import com.jtuffery.kardtest.screens.transactions.domain.repository.TransactionsRepository
import com.jtuffery.kardtest.screens.transactions.domain.usecase.getTransactionsUseCaseFactory
import com.jtuffery.kardtest.screens.transactions.network.data.NetworkTransactionsDataSource
import com.jtuffery.kardtest.screens.transactions.presentation.TransactionsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val transactionsNetworkModule = module {
    single<TransactionsDataSource> { NetworkTransactionsDataSource(get(), get(named(DATE_TIME_FORMATTER_ISO8601))) }
}

val transactionsDataModule = module {
    single<TransactionsRepository> { DefaultTransactionsRepository(get()) }
}

val transactionsDomainModule = module {
    factory { getTransactionsUseCaseFactory(get()) }
}

val transactionsPresentationModule = module {
    viewModel { TransactionsViewModel(get<Context>().resources, get()) }
}

val transactionsScreenModules = transactionsNetworkModule +
        transactionsDataModule +
        transactionsDomainModule +
        transactionsPresentationModule