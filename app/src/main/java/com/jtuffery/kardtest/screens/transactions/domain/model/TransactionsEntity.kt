package com.jtuffery.kardtest.screens.transactions.domain.model

data class TransactionsEntity(
    val firstName: String,
    val currentBalanceValue: Float,
    val currentBalanceSymbol: String,
    val userTransactionsList: List<UserTransactionEntity>,
    val friendsTransactionsList: List<FriendTransactionEntity>
)