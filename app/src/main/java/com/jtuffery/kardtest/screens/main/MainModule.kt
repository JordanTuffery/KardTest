package com.jtuffery.kardtest.screens.main

import com.jtuffery.kardtest.screens.main.presentation.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import org.threeten.bp.format.DateTimeFormatter

const val DATE_TIME_FORMATTER_ISO8601 = "DATE_TIME_FORMATTER_ISO8601"

val mainModule = module {
    single(named(DATE_TIME_FORMATTER_ISO8601)) { DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'") }
    viewModel { MainViewModel(get()) }
}