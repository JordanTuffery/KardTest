package com.jtuffery.kardtest.screens.main.presentation

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.jtuffery.kardtest.screens.kardeals.KarDealFragment
import com.jtuffery.kardtest.screens.profile.ProfileFragment
import com.jtuffery.kardtest.screens.transactions.presentation.TransactionsFragment
import java.lang.IllegalArgumentException

class MainViewPagerAdapter(
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle
): FragmentStateAdapter(fragmentManager, lifecycle) {
    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment = when(position) {
        Screen.KarDeals.ordinal -> KarDealFragment()
        Screen.Transactions.ordinal -> TransactionsFragment()
        Screen.Profile.ordinal -> ProfileFragment()
        else -> throw IllegalArgumentException("We shouldn't be able to swipe at an other ordinal.")
    }

    enum class Screen {
        KarDeals,
        Transactions,
        Profile
    }
}