package com.jtuffery.kardtest.screens.transactiondetails.presentation

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jtuffery.kardtest.R
import com.jtuffery.kardtest.common.UiState
import com.jtuffery.kardtest.common.observe
import com.jtuffery.kardtest.core.refresh.domain.usecase.RefreshFlow
import com.jtuffery.kardtest.core.refresh.presentation.RefreshViewModel
import com.jtuffery.kardtest.databinding.FragmentTransactionDetailsBinding
import com.jtuffery.kardtest.designsystem.ErrorComposable
import com.jtuffery.kardtest.designsystem.LoaderComposable
import com.jtuffery.kardtest.theme.KardTheme
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class TransactionDetailsBottomDialogFragment : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentTransactionDetailsBinding

    private val transactionDetailsViewModel: TransactionDetailsViewModel by viewModel()
    private val refreshViewModel: RefreshViewModel by sharedViewModel()

    private val refreshFlow: RefreshFlow by inject()

    private val id: String by lazy { requireArguments().getString(TRANSACTION_ID)!! }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_transaction_details, container, false
        )
        binding.composeviewLoader.setContent {
            KardTheme {
                LoaderComposable()
            }
        }
        binding.composeviewError.setContent {
            KardTheme {
                ErrorComposable(refreshViewModel::refresh)
            }
        }
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setOnShowListener {
            val bottomSheetDialog = dialog as? BottomSheetDialog
            val bottomSheetView =
                bottomSheetDialog?.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)
            bottomSheetView?.let {
                BottomSheetBehavior.from(it).setState(BottomSheetBehavior.STATE_EXPANDED)
            }
        }
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        transactionDetailsViewModel.uiStateFlow.observe(viewLifecycleOwner) {
            when (it) {
                is UiState.Success -> binding.uim = it.uim
                else -> Unit
            }
            binding.uiState = it
        }

        refreshFlow.observe(viewLifecycleOwner) {
            transactionDetailsViewModel.refresh(id)
        }

        lifecycleScope.launchWhenResumed {
            transactionDetailsViewModel.refresh(id)
        }
    }

    companion object {
        private const val TRANSACTION_ID = "TRANSACTION_ID"
        fun newInstance(id: String): TransactionDetailsBottomDialogFragment =
            TransactionDetailsBottomDialogFragment().apply {
                arguments = bundleOf(
                    TRANSACTION_ID to id
                )
            }
    }
}
