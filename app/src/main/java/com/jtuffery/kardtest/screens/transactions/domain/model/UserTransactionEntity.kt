package com.jtuffery.kardtest.screens.transactions.domain.model

import org.threeten.bp.LocalDateTime

data class UserTransactionEntity(
    val categoryUrl: String,
    val categoryName: String,
    val transactionDate: LocalDateTime,
    val title: String,
    val id: String,
    val amountValue: Float,
    val amountSymbol: String,
)