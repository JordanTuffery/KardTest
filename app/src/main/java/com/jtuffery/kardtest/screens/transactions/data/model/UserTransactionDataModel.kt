package com.jtuffery.kardtest.screens.transactions.data.model

import com.jtuffery.kardtest.screens.transactions.domain.model.UserTransactionEntity

typealias UserTransactionDataModel = UserTransactionEntity

fun UserTransactionDataModel.toEntity() = this