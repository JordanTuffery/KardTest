package com.jtuffery.kardtest.screens.transactions.domain.repository

import com.jtuffery.kardtest.common.Either
import com.jtuffery.kardtest.screens.transactions.domain.model.TransactionsEntity

interface TransactionsRepository {
    suspend fun getTransactions(): Either<Exception, TransactionsEntity>
}