package com.jtuffery.kardtest.screens.transactions.presentation

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jtuffery.kardtest.common.Either
import com.jtuffery.kardtest.common.UiState
import com.jtuffery.kardtest.screens.transactions.domain.usecase.GetTransactionsUseCase
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber

class TransactionsViewModel(
    private val resources: Resources,
    private val getTransactionsUseCase: GetTransactionsUseCase
): ViewModel() {

    private val _uiStateFlow: MutableStateFlow<UiState<TransactionsScreenUiModel>> =
        MutableStateFlow(UiState.Loading())
    val uiStateFlow: StateFlow<UiState<TransactionsScreenUiModel>> = _uiStateFlow

    private val _actionsFlow: MutableSharedFlow<Action> = MutableSharedFlow(0, 1)
    val actionsFlow = _actionsFlow

    fun refresh() {
        viewModelScope.launch {
            _uiStateFlow.value = UiState.Loading()
            val result = getTransactionsUseCase()
            _uiStateFlow.value = when (result) {
                is Either.Success -> {
                    UiState.Success(result.value.toUiModel(
                        resources,
                        ::onClickAdd,
                        ::onClickSend,
                        ::onClickAddGooglePay,
                        ::onItemClick
                    ))
                }
                is Either.Failure -> {
                    UiState.Error(result.value)
                }
            }
        }
    }

    private fun onClickAdd() {
        Timber.e("Add Money !")
    }

    private fun onClickSend() {
        Timber.e("Send Money !")
    }

    private fun onClickAddGooglePay() {
        Timber.e("Add Google Pay !")
    }

    private fun onItemClick(id: String) {
        _actionsFlow.tryEmit(
            Action.OpenDetails(id)
        )
    }

    sealed class Action {
        data class OpenDetails(val id: String): Action()
    }
}

