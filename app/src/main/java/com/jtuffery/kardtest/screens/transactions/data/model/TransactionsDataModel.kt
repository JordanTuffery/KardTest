package com.jtuffery.kardtest.screens.transactions.data.model

import com.jtuffery.kardtest.screens.transactions.domain.model.TransactionsEntity

typealias TransactionsDataModel = TransactionsEntity

fun TransactionsDataModel.toEntity() = this