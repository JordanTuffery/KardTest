package com.jtuffery.kardtest.screens.main.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jtuffery.kardtest.common.Either
import com.jtuffery.kardtest.common.UiState
import com.jtuffery.kardtest.core.refresh.domain.usecase.RefreshFlow
import com.jtuffery.kardtest.screens.main.domain.usecase.GetUserAvatarUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class MainViewModel(
    private val getUserAvatarUseCase: GetUserAvatarUseCase
) : ViewModel() {

    private val _uim: MutableStateFlow<UiState<MainUiModel>> = MutableStateFlow(UiState.Loading())
    val uim: StateFlow<UiState<MainUiModel>> = _uim

    fun refreshAvatar() {
        viewModelScope.launch {
            _uim.value = UiState.Loading()
            val result = getUserAvatarUseCase()
            val url = if(result is Either.Success) {
                result.value
            } else {
                null
            }

            _uim.value = UiState.Success(MainUiModel(url))
        }
    }
}