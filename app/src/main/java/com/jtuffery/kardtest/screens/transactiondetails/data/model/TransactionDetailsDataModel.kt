package com.jtuffery.kardtest.screens.transactiondetails.data.model

import com.jtuffery.kardtest.screens.transactiondetails.domain.model.TransactionDetailsEntity

typealias TransactionDetailsDataModel = TransactionDetailsEntity

fun TransactionDetailsDataModel.toEntity(): TransactionDetailsEntity = this