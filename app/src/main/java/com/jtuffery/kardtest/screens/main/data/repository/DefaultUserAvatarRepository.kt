package com.jtuffery.kardtest.screens.main.data.repository

import com.jtuffery.kardtest.common.Either
import com.jtuffery.kardtest.screens.main.data.datasource.UserAvatarDataSource
import com.jtuffery.kardtest.screens.main.domain.repository.UserAvatarRepository

class DefaultUserAvatarRepository(
    private val dataSource: UserAvatarDataSource
): UserAvatarRepository {
    override suspend fun getAvatar(): Either<Exception, String> =
        dataSource.getAvatar()
}