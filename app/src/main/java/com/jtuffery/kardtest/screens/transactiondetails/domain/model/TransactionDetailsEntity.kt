package com.jtuffery.kardtest.screens.transactiondetails.domain.model

import org.threeten.bp.LocalDateTime

data class TransactionDetailsEntity(
    val categoryUrl: String,
    val categoryName: String,
    val title: String,
    val amountValue: Float,
    val amountSymbol: String,
    val avatarUrl: String?,
    val firstName: String,
    val lastName: String,
    val transactionDate: LocalDateTime,
    val commentsCount: Int,
    val linkedImgUrl: String?
)