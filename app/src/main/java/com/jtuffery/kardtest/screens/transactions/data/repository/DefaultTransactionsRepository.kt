package com.jtuffery.kardtest.screens.transactions.data.repository

import com.jtuffery.kardtest.common.Either
import com.jtuffery.kardtest.screens.transactions.data.datasource.TransactionsDataSource
import com.jtuffery.kardtest.screens.transactions.data.model.toEntity
import com.jtuffery.kardtest.screens.transactions.domain.model.TransactionsEntity
import com.jtuffery.kardtest.screens.transactions.domain.repository.TransactionsRepository

class DefaultTransactionsRepository(
    private val dataSource: TransactionsDataSource
) : TransactionsRepository {
    override suspend fun getTransactions(): Either<Exception, TransactionsEntity> =
        dataSource.getTransactions().mapSuccess { it.toEntity() }
}