package com.jtuffery.kardtest.screens.transactions.data.datasource

import com.jtuffery.kardtest.common.Either
import com.jtuffery.kardtest.screens.transactions.data.model.TransactionsDataModel

interface TransactionsDataSource {
    suspend fun getTransactions(): Either<Exception, TransactionsDataModel>
}