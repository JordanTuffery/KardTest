package com.jtuffery.kardtest.screens.transactions.domain.model

import org.threeten.bp.LocalDateTime

data class FriendTransactionEntity(
    val id: String,
    val transactionDate: LocalDateTime,
    val categoryUrl: String,
    val avatarUrl: String,
    val firstName: String,
    val lastName: String,
    val title: String,
)