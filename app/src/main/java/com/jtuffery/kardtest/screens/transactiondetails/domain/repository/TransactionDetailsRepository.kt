package com.jtuffery.kardtest.screens.transactiondetails.domain.repository

import com.jtuffery.kardtest.common.Either
import com.jtuffery.kardtest.screens.transactiondetails.domain.model.TransactionDetailsEntity

interface TransactionDetailsRepository {
    suspend fun getTransactionDetails(id: String): Either<Exception, TransactionDetailsEntity>
}