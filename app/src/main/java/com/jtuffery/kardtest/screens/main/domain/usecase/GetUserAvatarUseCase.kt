package com.jtuffery.kardtest.screens.main.domain.usecase

import com.jtuffery.kardtest.common.Either
import com.jtuffery.kardtest.screens.main.domain.repository.UserAvatarRepository

interface GetUserAvatarUseCase {
    suspend operator fun invoke(): Either<Exception, String>
}

fun getUserAvatarUseCaseFactory(repository: UserAvatarRepository) =
    object : GetUserAvatarUseCase {
        override suspend fun invoke(): Either<Exception, String> = repository.getAvatar()
    }