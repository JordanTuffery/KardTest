package com.jtuffery.kardtest.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

private val LightColorPalette = lightColors(
        primary = Purple200,
        primaryVariant = Purple500
)

@Composable
fun KardTheme(content: @Composable() () -> Unit) {
    val colors = LightColorPalette

    MaterialTheme(
            colors = colors,
            shapes = Shapes,
            content = content
    )
}