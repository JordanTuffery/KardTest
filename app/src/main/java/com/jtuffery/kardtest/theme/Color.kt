package com.jtuffery.kardtest.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFF7700F0)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF6001C1)
val DarkPurple1 = Color(0xFF1F193F)
val Green200 = Color(0xFF3CE976)
val Blue200 = Color(0xFF35C4FF)