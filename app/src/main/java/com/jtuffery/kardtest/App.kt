package com.jtuffery.kardtest

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import com.jtuffery.kardtest.core.refresh.refreshModules
import com.jtuffery.kardtest.network.networkModule
import com.jtuffery.kardtest.screens.main.mainModule
import com.jtuffery.kardtest.screens.main.userAvatarCoreModules
import com.jtuffery.kardtest.screens.transactiondetails.transactionDetailsModules
import com.jtuffery.kardtest.screens.transactions.transactionsScreenModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)

        initTimber()
        initKoin()
    }

    private fun initTimber() {
        val tree = if (BuildConfig.DEBUG) {
            Timber.DebugTree()
        } else {
            NoLoggingTree()
        }

        Timber.plant(tree)
    }

    private fun initKoin() {
        val coreModules = userAvatarCoreModules + refreshModules
        val screenModules = transactionsScreenModules +
                transactionDetailsModules

        startKoin {
            androidContext(this@App)
            modules(
                networkModule +
                        coreModules +
                        screenModules +
                        mainModule
            )
        }
    }
}