package com.jtuffery.kardtest.designsystem

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.dimensionResource
import com.jtuffery.kardtest.R
import com.jtuffery.kardtest.theme.DarkPurple1

@Composable
fun ChipButton(
    icon: ImageVector,
    iconContentDescription: String,
    tint: Color,
    text: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Surface(
        color = DarkPurple1,
        contentColor = Color.White,
        modifier = modifier
            .shadow(dimensionResource(id = R.dimen.gap_xs), MaterialTheme.shapes.small)
            .clip(MaterialTheme.shapes.small)
            .clickable { onClick() }
    ) {
        Row(
            Modifier.padding(dimensionResource(id = R.dimen.gap_s))
        ) {
            Icon(
                imageVector = icon,
                contentDescription = iconContentDescription,
                modifier = Modifier
                    .size(dimensionResource(id = R.dimen.gap_l))
                    .align(Alignment.CenterVertically)
                    .padding(
                        start = dimensionResource(id = R.dimen.gap_xs),
                        end = dimensionResource(id = R.dimen.gap_xxs)
                    ),
                tint = tint
            )
            Text(
                text = text,
                modifier = Modifier
                    .padding(
                        start = dimensionResource(id = R.dimen.gap_xxs),
                        end = dimensionResource(id = R.dimen.gap_xs)
                    )
            )
        }
    }
}