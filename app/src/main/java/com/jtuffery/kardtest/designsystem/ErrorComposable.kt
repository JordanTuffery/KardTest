package com.jtuffery.kardtest.designsystem

import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material.icons.filled.WifiOff
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.jtuffery.kardtest.R
import com.jtuffery.kardtest.theme.Blue200

@Composable
fun ErrorComposable(
    onRetryClick: () -> Unit
) {
    Box(
        Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        Column(
            Modifier.align(Alignment.Center)
        ) {
            Icon(
                imageVector = Icons.Filled.WifiOff,
                contentDescription = "No Wifi",
                modifier = Modifier
                    .size(85.dp)
                    .align(Alignment.CenterHorizontally)
            )
            Text(
                text = stringResource(id = R.string.error_no_internet_title),
                style = MaterialTheme.typography.h5,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .padding(top = dimensionResource(id = R.dimen.gap_m))
                    .fillMaxWidth(0.7f)
            )
            ChipButton(
                icon = Icons.Filled.Refresh,
                iconContentDescription = "Retry Button",
                tint = Blue200,
                text = stringResource(id = R.string.error_no_internet_retry_button),
                onClick = { onRetryClick() },
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(top = dimensionResource(id = R.dimen.gap_m))
            )
        }
    }
}