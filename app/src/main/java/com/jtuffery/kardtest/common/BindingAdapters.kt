package com.jtuffery.kardtest.common

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.load
import coil.transform.CircleCropTransformation
import coil.transform.RoundedCornersTransformation

@BindingAdapter(value = ["url", "shouldCropCircle", "cornerRadius"], requireAll = false)
fun ImageView.setUrl(
    url: String? = null,
    shouldCircleCrop: Boolean? = null,
    cornerRadius: Float? = null
) {
    if(url == null) return
    load(url) {
        crossfade(true)
        if(shouldCircleCrop == true) {
            transformations(CircleCropTransformation())
        } else if(cornerRadius != null) {
            transformations(RoundedCornersTransformation(cornerRadius))
        }
    }
}

@BindingAdapter("visibleOrGone")
fun View.setVisibleOrGone(
    visibleOrGone: Boolean
) {
    visibility = if(visibleOrGone) View.VISIBLE else View.GONE
}