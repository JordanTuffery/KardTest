package com.jtuffery.kardtest.common

import android.content.res.Resources
import com.jtuffery.kardtest.R
import org.threeten.bp.LocalDateTime
import org.threeten.bp.Month
import org.threeten.bp.temporal.ChronoField
import org.threeten.bp.temporal.ChronoUnit

fun Month.toMonthString(resources: Resources) =
    resources.getString(when(this) {
        Month.JANUARY -> R.string.january
        Month.FEBRUARY -> R.string.february
        Month.MARCH -> R.string.march
        Month.APRIL -> R.string.april
        Month.MAY -> R.string.may
        Month.JUNE -> R.string.june
        Month.JULY -> R.string.july
        Month.AUGUST -> R.string.august
        Month.SEPTEMBER -> R.string.september
        Month.OCTOBER -> R.string.october
        Month.NOVEMBER -> R.string.november
        Month.DECEMBER -> R.string.december
    })

fun LocalDateTime.isToday(): Boolean {
    val today = LocalDateTime.now()
    val isDaySame = dayOfMonth == today.dayOfMonth
    val isMonthSame = monthValue == today.monthValue
    val isYearTheSame = year == today.year
    return isDaySame && isMonthSame && isYearTheSame
}

fun LocalDateTime.isYesterday(): Boolean {
    val yesterday = LocalDateTime.now().minus(1, ChronoUnit.DAYS)
    val isDaySame = dayOfMonth == yesterday.dayOfMonth
    val isMonthSame = monthValue == yesterday.monthValue
    val isYearTheSame = year == yesterday.year
    return isDaySame && isMonthSame && isYearTheSame
}

fun LocalDateTime.isLastWeek(): Boolean {
    val lastWeek = LocalDateTime.now().minus(1, ChronoUnit.WEEKS)
    return get(ChronoField.ALIGNED_WEEK_OF_YEAR) == lastWeek.get(ChronoField.ALIGNED_WEEK_OF_YEAR)
}

fun LocalDateTime.isLast2Week(): Boolean {
    val last2Week = LocalDateTime.now().minus(2, ChronoUnit.WEEKS)
    return get(ChronoField.ALIGNED_WEEK_OF_YEAR) == last2Week.get(ChronoField.ALIGNED_WEEK_OF_YEAR)
}

fun LocalDateTime.isRemainingThisMonth(): Boolean {
    val today = LocalDateTime.now()
    val isMonthSame = monthValue == today.monthValue
    val isYearTheSame = year == today.year
    return !isToday() && !isYesterday() && !isLastWeek() && !isLast2Week() && isMonthSame && isYearTheSame
}

fun LocalDateTime.isLastMonth(): Boolean {
    val lastMonth = LocalDateTime.now().minus(1, ChronoUnit.MONTHS)
    val isMonthSame = monthValue == lastMonth.monthValue
    val isYearTheSame = year == lastMonth.year
    return isMonthSame && isYearTheSame
}

fun LocalDateTime.isBeforeLastMonth(): Boolean {
    return !isToday() && !isYesterday() && !isLastWeek() && !isLast2Week() && !isRemainingThisMonth() && !isLastMonth()
}