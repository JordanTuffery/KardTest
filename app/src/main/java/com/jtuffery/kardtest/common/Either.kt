package com.jtuffery.kardtest.common

sealed class Either<out F, out S> {
    data class Failure<out T>(val value: T) : Either<T, Nothing>()
    data class Success<out T>(val value: T) : Either<Nothing, T>()

    inline fun <X> fold(fl: (F) -> X, fr: (S) -> X): X = when (this) {
        is Success -> fr(value)
        is Failure -> fl(value)
    }

    inline fun <X> mapSuccess(f: (S) -> X): Either<F, X> = when (this) {
        is Success -> Success(f(value))
        is Failure -> Failure(value)
    }

    inline fun <X> mapFailure(f: (F) -> X): Either<X, S> = when (this) {
        is Success -> Success(value)
        is Failure -> Failure(f(value))
    }

    inline fun alsoSuccess(block: (S) -> Unit): Either<F, S>  {
        when (this) {
            is Success -> block(this.value)
        }
        return this
    }

    inline fun alsoFailure(block: (F) -> Unit): Either<F, S>  {
        when (this) {
            is Failure -> block(this.value)
        }
        return this
    }

    companion object {
        inline fun <T> catch(body: () -> T): Either<Throwable, T> = try {
            Success(body())
        } catch (t: Throwable) {
            Failure(t)
        }
    }
}

fun <T> Either<T, T>.merge(): T = when (this) {
    is Either.Success -> value
    is Either.Failure -> value
}
fun <F, S> Either<F, S>.getOrElse(default: S): S = when (this) {
    is Either.Success -> value
    is Either.Failure -> default
}

inline fun <F, S, X> Either<F, S>.flatMap(f: (S) -> Either<F, X>): Either<F, X> = when (this) {
    is Either.Failure -> this
    is Either.Success -> f(value)
}