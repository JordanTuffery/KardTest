package com.jtuffery.kardtest.common

sealed class UiState<T> {
    class Loading<T> : UiState<T>()
    data class Error<T>(val exception: Exception) : UiState<T>()
    data class Success<T>(val uim: T) : UiState<T>()

    fun isLoading() = this is Loading
    fun isError() = this is Error
    fun isSuccess() = this is Success
}