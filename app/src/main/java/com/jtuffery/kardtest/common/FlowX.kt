package com.jtuffery.kardtest.common

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

fun <T> Flow<T>.observe(
    owner: LifecycleOwner,
    minState: Lifecycle.State = Lifecycle.State.STARTED,
    block: suspend (T) -> Unit
) {
    onEach {
        if (owner.lifecycle.currentState.isAtLeast(minState)) {
            block(it)
        }
    }.launchIn(owner.lifecycle.coroutineScope)
}