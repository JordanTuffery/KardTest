package com.jtuffery.kardtest.network

import com.apollographql.apollo.api.Response
import com.jtuffery.kardtest.common.Either

suspend fun <T> launchRequest(call: suspend () -> Response<T>): Either<Exception, T> {
    try {
        val response = call()
        val result = response.data
        return if(response.hasErrors() || result == null) {
            Either.Failure(NetworkException())
        } else {
            Either.Success(result)
        }
    } catch(e: Exception) {
        return Either.Failure(NetworkException(e))
    }
}

class NetworkException(e: Exception? = null): Exception("network error", e)