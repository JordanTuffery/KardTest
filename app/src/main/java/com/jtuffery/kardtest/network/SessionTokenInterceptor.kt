package com.jtuffery.kardtest.network

import okhttp3.Interceptor
import okhttp3.Response

class SessionTokenInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequest = chain.request()
            .newBuilder()
            .addHeader(HEADER_AUTHORIZATION, "Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwZTU5ODRhNi1mYjgxLTQwNzgtOGFjNi1jY2U1ZjFiNDZkNzQiLCJzdWIiOiJkZTdhMjA2OC1lMWNlLTQwZWEtOGRlMC04MDZiYWYyYzM2OTAiLCJzY3AiOiJ1c2VyIiwiYXVkIjpudWxsLCJpYXQiOjE2MzI0OTAyODcsImV4cCI6MTYzMzA5NTA4N30.8RUfq4Epsrsuvk4TTVLHEOlDfT6rKwln39TpOHiQRsY")
            .build()
        return chain.proceed(newRequest)
    }

    companion object {
        private const val HEADER_AUTHORIZATION = "Authorization"
    }
}