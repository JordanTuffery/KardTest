package com.jtuffery.kardtest.network

import com.apollographql.apollo.ApolloClient
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import timber.log.Timber

val networkModule = module {
    single {
        val logger = HttpLoggingInterceptor.Logger { message -> Timber.d(message) }
        val loggingInterceptor = HttpLoggingInterceptor(logger)
        OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(SessionTokenInterceptor())
            .build()
    }

    single {
        ApolloClient.builder()
            .serverUrl("https://staging.kard.eu/graphql")
            .okHttpClient(get())
            .build()
    }
}