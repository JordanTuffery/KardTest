plugins {
    id(Plugins.application)
    id(Plugins.kotlinAndroid)
    `kotlin-kapt`
    id(Plugins.Apollo.id).version(Plugins.Apollo.version)
}

android {
    compileSdk = Config.compileSdk

    defaultConfig {
        applicationId = Config.applicationId
        minSdk = Config.minSdk
        targetSdk = Config.targetSdk
        versionCode = Config.versionCode
        versionName = Config.versionName

        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        getByName("debug") {
            isDebuggable = true
        }
        getByName("release") {
            isMinifyEnabled = false
            isDebuggable = false
                proguardFiles(
                    getDefaultProguardFile("proguard-android-optimize.txt"),
                    "proguard-rules.pro"
                )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    composeOptions {
        kotlinCompilerExtensionVersion = Dependencies.Compose.Core.version
    }
    buildFeatures.compose = true
    dataBinding.isEnabled = true
}

dependencies {
    implementation(Dependencies.AndroidX.core)
    implementation(Dependencies.AndroidX.AppCompat.core)
    implementation(Dependencies.AndroidX.ConstraintLayout.core)
    implementation(Dependencies.Google.Material.core)

    implementation(Dependencies.Compose.Core.material)
    implementation(Dependencies.Compose.Core.ui)
    implementation(Dependencies.Compose.Core.icons)
    implementation(Dependencies.Compose.Core.toolingDebug)
    implementation(Dependencies.Compose.Core.toolingPreview)

    implementation(Dependencies.Compose.Coil.compose)

    implementation(Dependencies.Compose.Activity.activity)

    implementation(Dependencies.AndroidX.Lifecycle.runtime)
    implementation(Dependencies.AndroidX.Lifecycle.viewModels)
    implementation(Dependencies.AndroidX.Lifecycle.liveData)

    implementation(Dependencies.AndroidX.Navigation.core)

    implementation(Dependencies.Kotlin.Coroutines.core)

    implementation(Dependencies.Apollo.runtime)
    implementation(Dependencies.Apollo.coroutine)

    implementation(Dependencies.Koin.android)

    implementation(Dependencies.OkHttp.core)
    implementation(Dependencies.OkHttp.interceptor)

    implementation(Dependencies.Timber.core)
    implementation(Dependencies.ThreeTen.core)
}

apollo {
    packageName.set(Config.applicationId)
    generateKotlinModels.set(true)
}