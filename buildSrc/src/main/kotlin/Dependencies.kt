object Dependencies {
    object AndroidX {
        private const val version = "1.6.0"
        const val core = "androidx.core:core-ktx:$version"

        object AppCompat {
            private const val version = "1.3.1"
            const val core = "androidx.appcompat:appcompat:$version"
        }

        object ConstraintLayout {
            private const val version = "2.1.0"
            const val core = "androidx.constraintlayout:constraintlayout:$version"
        }

        object Lifecycle {
            private const val version = "2.3.1"
            const val runtime =  "androidx.lifecycle:lifecycle-runtime-ktx:$version"
            const val viewModels =  "androidx.lifecycle:lifecycle-viewmodel-ktx:$version"
            const val liveData =  "androidx.lifecycle:lifecycle-livedata-ktx:$version"
        }

        object Navigation {
            private const val version = "2.3.5"
            const val core = "androidx.navigation:navigation-fragment-ktx:$version"
        }
    }

    object Google {
        object Material {
            private const val version = "1.4.0"
            const val core = "com.google.android.material:material:$version"
        }
    }

    object Gradle {
        private const val version = "7.0.2"
        const val tools = "com.android.tools.build:gradle:$version"
    }

    object Kotlin {
        private const val version = "1.5.30"
        const val gradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$version"

        object Coroutines {
            private const val version = "1.3.9"
            const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$version"
        }
    }

    object Apollo {
        const val version = "2.5.9"
        const val runtime = "com.apollographql.apollo:apollo-runtime:$version"
        const val coroutine = "com.apollographql.apollo:apollo-coroutines-support:$version"
    }

    object Koin {
        private const val version ="3.1.2"
        const val android = "io.insert-koin:koin-android:$version"
    }

    object OkHttp {
        private const val version ="4.9.0"
        const val core = "com.squareup.okhttp3:okhttp:$version"
        const val interceptor = "com.squareup.okhttp3:logging-interceptor:$version"
    }

    object Timber {
        private const val version = "5.0.1"
        const val core = "com.jakewharton.timber:timber:$version"
    }

    object Compose {
        object Core {
            const val version = "1.0.2"
            const val ui = "androidx.compose.ui:ui:$version"
            const val material = "androidx.compose.material:material:$version"
            const val icons = "androidx.compose.material:material-icons-extended:$version"
            const val toolingPreview = "androidx.compose.ui:ui-tooling-preview:$version"
            const val toolingDebug = "androidx.compose.ui:ui-tooling:$version"
        }

        object Activity {
            private const val version = "1.3.1"
            const val activity = "androidx.activity:activity-compose:$version"
        }

        object Coil {
            private const val version = "1.3.2"
            const val compose = "io.coil-kt:coil-compose:$version"
        }
    }

    object ThreeTen {
        private const val version = "1.3.1"
        const val core = "com.jakewharton.threetenabp:threetenabp:$version"
    }
}