object Plugins {
    const val application = "com.android.application"
    const val kotlinAndroid = "kotlin-android"
    object Apollo {
        const val version = Dependencies.Apollo.version
        const val id = "com.apollographql.apollo"
    }
}