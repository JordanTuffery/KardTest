plugins {
    `kotlin-dsl`
    `java-gradle-plugin`
    `embedded-kotlin`
}

repositories {
    google()
    mavenCentral()
    jcenter()
}

dependencies {
    implementation("com.android.tools.build:gradle:7.0.1")
    implementation(kotlin("gradle-plugin", "1.5.21"))
}